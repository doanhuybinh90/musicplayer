//
//  ViewController.swift
//  MusicPlayer
//
//  Created by Doan Huy Binh on 9/27/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import AVFoundation



class ViewController: UIViewController,AVAudioPlayerDelegate {

    var MusicFiles = [String]()

    var musicPlayer: AVAudioPlayer = AVAudioPlayer()
    var currentIndex: Int = 0
    
    var timer: Timer = Timer()
    var timeRemaining: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMusicFiles()
        songNameLabel.text = ""
        timeLabel.text = "00:00"
        playMusic()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ViewController.updateSlider), userInfo: nil, repeats: true)
    }
    
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    @IBOutlet weak var musicSlider: UISlider!
    @IBOutlet weak var volumeSlider: UISlider!

    
    
    func loadMusicFiles()
    {
        let ResourcePath = Bundle.main.resourcePath!
        var directoryContents = [NSString]()
        
        do
        {
            directoryContents = try FileManager.default.contentsOfDirectory(atPath: ResourcePath) as [NSString]
        }
        catch _
        {
            print("Error in fetching Directory Contents")
        }
        
        for i in 1...directoryContents.count - 1
        {
            let fileExtendsion: String = (directoryContents[i] as NSString).pathExtension
            
            if  fileExtendsion == "mp3"
            {
                let fileName: String  = (directoryContents[i] as NSString).deletingPathExtension
                MusicFiles.append(fileName)
            }
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }
    func playMusic()
    {
        let filePath = NSString(string: Bundle.main.path(forResource: MusicFiles[currentIndex], ofType: "mp3")!)
        
        let fileURL = NSURL(fileURLWithPath: filePath as String)
        
        do
        {
            musicPlayer = try AVAudioPlayer(contentsOf: fileURL as URL)
        }
        catch _
        {
            print("Error initialing the Music Player")
        }
        
        musicPlayer.delegate = self
        musicSlider.minimumValue = 0
        musicSlider.maximumValue = Float(musicPlayer.duration)
        
        musicSlider.value = Float(musicPlayer.currentTime)
        musicPlayer.volume = volumeSlider.value

        musicPlayer.play()
        songNameLabel.text = MusicFiles[currentIndex]
    }
    
    
    @IBAction func back(_ sender: AnyObject) {
        currentIndex = currentIndex - 1
        if currentIndex < 0
        {
            currentIndex = MusicFiles.count - 1
        }
        playMusic()
    }

    @IBAction func next(_ sender: AnyObject) {
        currentIndex = currentIndex + 1
        if currentIndex == MusicFiles .count
        {
            currentIndex = 0
        }
       playMusic()
    }
    
    @IBAction func play(_ sender: AnyObject) {
        musicPlayer.play()
        songNameLabel.text = MusicFiles[currentIndex]
    }
    
    @IBAction func pause(_ sender: AnyObject) {
        musicPlayer.pause()
    }
    
    @IBAction func stop(_ sender: AnyObject) {
        musicPlayer.stop()
        songNameLabel.text = ""
        timeLabel.text = "00:00"
    }
    
    @IBAction func timeButton(_ sender: AnyObject) {
        timeRemaining = !timeRemaining
    }
    
    func updateSlider()
    {
        volumeSlider.value = Float(musicPlayer.currentTime)
        if timeRemaining == false
        {
            timeLabel.text = updateTime(currentTime: musicPlayer.currentTime)
        }
        else
        {
            timeLabel.text = updateTime(currentTime: musicPlayer.duration  - musicPlayer.currentTime)
        }
    }
    
    func updateTime(currentTime: TimeInterval) ->String
    {
        let current: Int = Int(currentTime)
        let minutes = current / 60
        let seconds = current % 60
        
        let minutesString = minutes > 9 ? "\(minutes)" : "0\(minutes)"
        let secondsString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
        
        if timeRemaining == false
        {
            return minutesString + ":" + secondsString
        }
        else
        {
            return "-" + minutesString + ":" + secondsString
        }
    }
    
    @IBAction func musicSliderChange(_ sender: AnyObject) {
        if musicPlayer.isPlaying
        {
            musicPlayer.currentTime = TimeInterval(musicSlider.value)
        }
    }
    
    @IBAction func volumeSliderChange(_ sender: AnyObject) {
        musicPlayer.volume = volumeSlider.value
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let random = arc4random_uniform(UInt32(MusicFiles.count))
        currentIndex = Int(random)
        playMusic()
    }
    func animatesongNameLabel()
    {
       UIView.animate(withDuration: 1, delay: 0.5, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: {() ->Void in 
        self.songNameLabel.alpha = 0
        }, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

